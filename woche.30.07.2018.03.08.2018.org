#+TITLE: Wochenbericht: Modul 2.3 Beschaffung
#+DATE: 23.07.2018 bis 27.08.2018
#+Macro: Ausbildungsjahr 2018
#+AUTHOR: Björn Bidar
#+include: template.org


* Montag
+ Beschaffung
  + Bedarfsermittlung
  + Sekundär-Bedarf
  + Primär-Bedarf
  + Tertiär-Bedarf

* Dienstag
+ Inventur
+ Aufgaben
  + Bearbeiten
  + Besprechen
+ Eitco-Präsentation
 
* Mittwoch
+ Grundlagen Lager und Materialdisposition
+ Materialbestand überprüfen
+ Aufgaben
  + Bearbeiten
  + besprechen

* Donnerstag
+ Präsentation erstellen
+ Arbeitsrecht: Präsentation
  + Was bedeutet Kündigungsschutz
  + Ausnahmen
  + Fehlverhalten
* Freitag
+ Arbeitsrecht: Präsentation Wiederholung
+ lernen, Präsentation zusammenfassen
